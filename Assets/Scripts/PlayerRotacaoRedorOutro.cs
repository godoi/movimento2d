using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Rotacionar em volta de outro objeto
//Rigibody2D Kinematic
public class PlayerRotacaoRedorOutro : MonoBehaviour {
    
    [SerializeField]
    private GameObject outro;
    [SerializeField]
    private Vector3 rotacaoInicial;
    [SerializeField]
    private float velocidadeRotacao;

    void Start() {
        transform.eulerAngles = rotacaoInicial;
    }

    void Update() {
        if (Input.GetKey(KeyCode.Q)) {
            transform.RotateAround(outro.transform.position, Vector3.forward, velocidadeRotacao * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.E)) {
            // transform.RotateAround(outro.transform.position, -Vector3.forward, velocidadeRotacao * Time.deltaTime);
            transform.Rotate(Vector3.forward, velocidadeRotacao * Time.deltaTime);
        }
    }
}
