using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Esquema 4 - Rigid Body Kinematic ou sem Rigid Body - Transform Translate

public class PlayerMovimento4 : MonoBehaviour {
    public float moveSpeed = 3;

    //Coloque movimento que nao eh baseado em fisica aqui
    void Update() {
        if (Input.GetKey(KeyCode.RightArrow)) {
            transform.Translate(Vector2.right * (Time.deltaTime * moveSpeed));
        } else if (Input.GetKey(KeyCode.LeftArrow)) {
            transform.Translate(Vector2.left * (Time.deltaTime * moveSpeed));
        }
    }
}
