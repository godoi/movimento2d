using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Esquema 3 - Rigid Body Kinematic ou sem Rigid Body - Transform Position direto

public class PlayerMovimento3 : MonoBehaviour {

    public float moveSpeed = 3;

    //Coloque movimento que nao eh baseado em fisica aqui
    void Update() {
        if (Input.GetKey(KeyCode.RightArrow)) {
            transform.position += transform.right * (Time.deltaTime * moveSpeed);
            //100 * em 1 seg
            //1000 * em 1 seg
            // transform.position += transform.right * moveSpeed;
        } else if (Input.GetKey(KeyCode.LeftArrow)) {
            transform.position -= transform.right * (Time.deltaTime * moveSpeed);
            // transform.position += transform.right * -moveSpeed;
        }
    }
}