using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Esquema 2 - Rigid Body Dynamic - Adicionando Força (vai depender da massa do Rigid Body e da fricção)
// Rigid Body 
//     Kinematic -> Forças físicas não afetam o Rigid Body
//     Dynamic   -> Forças físicas afetam o Rigid Body

public class PlayerMovimento2 : MonoBehaviour {
    private Rigidbody2D rb2g;
    [Header("Movimento Jogador")]
    [SerializeField]
    private float moveSpeed = 3;
    [SerializeField]
    private float forcaPulo = 50;
    private const string RIGHT = "right";
    private const string LEFT = "left";
    private string buttonPressed;
    private bool pular = false;
   
    void Start() {
        rb2g = GetComponent<Rigidbody2D>();
    }

    //Coloque movimento que nao eh baseado em fisica aqui
    void Update() {
        if (Input.GetKey(KeyCode.RightArrow)) {
            buttonPressed = RIGHT;
        } else if (Input.GetKey(KeyCode.LeftArrow)) {
            buttonPressed = LEFT;
        } else {
            buttonPressed = null;
        }
        if (Input.GetKeyDown(KeyCode.Space)) {
            pular = true;
        }
        Debug.Log(buttonPressed);
    }

    //Coloque movimento baseado em fisica aqui
    private void FixedUpdate() {
        if (buttonPressed == RIGHT) {
            rb2g.AddForce(new Vector2(moveSpeed, 0));
            // rb2g.AddForce(new Vector2(moveSpeed, 0), ForceMode2D.Force);  // eh o padrao qdo nao colocado
            // rb2d.AddForce(new Vector2(moveSpeed, 0), ForceMode2D.Impulse);
        } else if (buttonPressed == LEFT) {
            rb2g.AddForce(new Vector2(-moveSpeed, 0));
        }
        if (pular) {
            rb2g.AddForce(new Vector2(0, forcaPulo));
            pular = false;
        }

    }
}
