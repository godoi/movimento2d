using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Esquema 1 - Rigid Body Dynamic – Velocidade
// Rigid Body 
//     Kinematic -> Forças físicas não afetam o Rigid Body
//     Dynamic   -> Forças físicas afetam o Rigid Body

public class PlayerMovimento1 : MonoBehaviour {
    private Rigidbody2D rb2g;
    public float moveSpeed = 3;
    private const string RIGHT = "right";
    private const string LEFT = "left";
    private string buttonPressed;

    void Start() {
        rb2g = GetComponent<Rigidbody2D>();
    }

    //Coloque movimento que nao eh baseado em fisica aqui
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow)) {
            buttonPressed = RIGHT;
        } else if (Input.GetKey(KeyCode.LeftArrow)) {
            buttonPressed = LEFT;
        } else {
            buttonPressed = null;
        }
        Debug.Log(buttonPressed);
    }

    //Coloque movimento baseado em fisica aqui
    private void FixedUpdate() {
        if (buttonPressed == RIGHT) {
            rb2g.velocity = new Vector2(moveSpeed, 0);
        } else if (buttonPressed == LEFT) {
            rb2g.velocity = new Vector2(-moveSpeed, 0);
        } else {
            rb2g.velocity = new Vector2(0, 0);
        }
    }
}
