using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Rotacionar em volta de si mesmo
//Rigibody2D Kinematic
public class PlayerRotacaoSiMesmo : MonoBehaviour {
    [SerializeField]
    private Vector3 rotacaoInicial;
    [SerializeField]
    private float velocidadeRotacao;

    // Start is called before the first frame update
    void Start() {
        transform.eulerAngles = rotacaoInicial;
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKey(KeyCode.Q)) {
            transform.Rotate(Vector3.forward, velocidadeRotacao * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.E)) {
            transform.Rotate(-Vector3.forward, velocidadeRotacao * Time.deltaTime);
        }
    }
}
