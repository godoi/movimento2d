using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Rigibody2D Dynamic
public class PlayerRotacaoTorque : MonoBehaviour
{
    private Rigidbody2D rb2g;
    [SerializeField]
    private float forcaRotacao;

    // Start is called before the first frame update
    void Start()
    {
        rb2g = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
         if (Input.GetKey(KeyCode.Q)) {
            rb2g.AddTorque(forcaRotacao);
        }
        if (Input.GetKey(KeyCode.E)) {
            rb2g.AddTorque(-forcaRotacao);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        Debug.Log("Encostou no chao");
    }
}
